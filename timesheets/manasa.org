* 2014

** May 2014

*** <2014-05-20 Tue>
  [In Time:10:30 am] [Out Time: 4:30pm]

-learned git commands
-introduction to python

*** <2014-05-21 Wed>
   [In Time:10:00am] [Out Time:4:40pm]

- learned emacs commands , literate programming
- introduction to python

*** <2014-05-22 Thur>
  [In Time:09:30am] [Out Time:5:20pm]

-tasks were discussed
-tasks were assigned
- an assignment in python was given
-completed python assignment
*** <2014-05-23 Fri>
  [In Time:9:30am] [Out Time:05:35pm]

-disscussion on python assignment and tasks assigned to me
-completed assignment in python

*** <2014-05-24 Sat>
    [In time :9:30] [Out time:5:15pm]

-worked on the given assignment

*** <2014-05-26 Mon>
    [In Time :9:00am] [Out Time: 6:00pm]

-worked on assign-2 python
-learned about openstack

*** <2014-05-27 Tue>
    [In Time : 09:30am] [Out Time: 06:10pm]

-worked on assgn-3
-attended lecture on virtual machine

*** <2014-05-27 Wed>
    [In Time : 09:30am] [Out Time : 06:30pm]

- completed assignment1 in python
- completed assignment2 in python
- completed assignment3 in python

*** <2014-05-28 Thur>
    [In Time: 09:30 am] [Out Time : 06:30pm]

-discussed abt python assignment-3
-installed VM , virtual lab

*** <2014-05-29 Fri>
    [In Time: 10:10 am] [Out Time: 06:00pm]

-installed openVZ,tested the working lab

*** <2014-05-31 Sat>
    [In Time : 10:00am] [Out Time: 01:00pm]

-read python.org

*** <2014-06-02 Mon>
    [In Time : 10:00am] [Out Time:06:00 pm]

-read abt logging in python
-GET & POST in python

*** <2014-06-03 Tue>
   [In Time : 09:33am] [Out Time:06:10pm]

-read abt loggers and handlers in logging  in python
-checked out the logging.py code

*** <2014-06-04 Wed>
    [In Time : 09:30am] [Out Time : 06:00pm]
- read abt GET and POST HTTP requests
- read abt filters and formatters in logging in python

*** <2014-06-05 Thur>
    [In Time : 09:30am] [Out Time :06:00pm]
-read the open vz commands
-checked centOSVZadptor.py

*** <2014-06-06 Fri>
    [In Time : 09:30am] [Out Time : 06:00pm]
-checked centOSVZadptor.py
-tried some codes in tornado

*** <2014-06-07 Sat>
    [In Time : 09:30am] [Out Time : 03:00pm]
- made a documentation on logging,get and post requests,open VZ

*** <2014-06-09 Mon>
    [In Time : 09:30am] [Out Time : 05:00pm]
- gave a presentation on what i have done so far
*** <2014-06-10 Tue>
    [In Time:10:25am] [Out Time:05:50pm]
- discussion abt the next task
*** <2014-06-11 Wed>
    [In Time:09:30am] [Out Time:05:30pm]
- discussion abt the structure of the ovpl with team mates, devi prasad sir, thirumal sir
*** <2014-06-12 Thur>
    [In Time:09:30am][Out Time:05:30pm]
-wrote scripts for destroying and checking the running containers in a VM
