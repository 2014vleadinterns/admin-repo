* 2014

** May 2014

*** <2014-05-20 Mon>
  [In Time: 9:00 a.m.] [Out Time: 6:00 p.m.]

- Attended the bootcamp
- Learned usage of git .
- Pushed the timesheet. 

*** <2014-05-21 Mon>
  [In Time: 10:00 a.m.] [Out Time: 5:00 p.m.]

- Learned EMACS.
- Learned ORG mode.
- Learned Literate Programming.
- Attended Python Session.
- Pushed the timesheet. 



*** <2014-05-22 Mon>
  [In Time: 9:00 a.m.] [Out Time: 6:00 p.m.]

- attended the task alloting presentation.
- worked on the python assignment .
- studied regarding OVPL dashboard.
- Pushed the timesheet. 

*** <2014-05-26 Mon>
  [In Time: 09:00 a.m.] [Out Time: 6:30 p.m.]

- Discussion on Python Assignments (Clipboard and Notes). 
- Worked on Assignment Problem on save_notes and on Three Address Instructions.
- Seminar on Virtualization, Cloud and OpenStack.
- studied about the Backend Rest API  on how to implement it and get info about vlabs dashboard .
- Pushed timesheet.


*** <2014-05-26 Mon>
  [In Time: 09:00 a.m.] [Out Time: 6:30 p.m.]

- Demo on virtual machine and containers.
- worked on the third assignment which has to be submitted by today. 
- studied about the Backend Rest API  on how to implement it and get info about vlabs dashboard .
- Pushed timesheet.


*** <2014-05-27 Tue>
  [In Time: 09:00 a.m.] [Out Time: 7:00 p.m.]

- Worked on Python Assignment Three Address Instructions.
- Attended a seminar on VMWare AND OpenVZ.
- Pushed timesheet.


*** <2014-05-28 Wed>
  [In Time: 9:30 a.m.] [Out Time: 8:30 p.m.]
  - Completed third assignment - 3-address code.
  - Installed apache server and worked on RESTful services.
  - Pushed the timesheet.


*** <2014-05-29 Wed>
  [In Time: 9:30 a.m.] [Out Time: 6:30 p.m.]
  - Discussion an Third assignment about unit testing.
  - Exploration about the dashboard and read about openvz and its commmands
  - Pushed the timesheet.


*** <2014-05-30 Fri>
  [In Time: 09:45 a.m.] [Out Time: 7:00 p.m.]

-Had a discussion regarding the Dashboard task.
-Working on python script to extract system info.
-Pushed Timesheet.


*** <2014-05-31 Sat>
  [In Time: 09:45 a.m.] [Out Time: 2:30 p.m.]

-Worked on getting familiar with RESTful web services.
-Studied about XMLHttpRequest().
-Pushed Timesheet.


*** <2014-06-02 Mon>
  [In Time: 09:50 a.m.] [Out Time: 6:30 p.m.]

-Worked on JSON file parsing and accessing elements of JS objects.
-Pushed Timesheet.


*** <2014-06-03 Tue>
  [In Time: 09:20 a.m.] [Out Time: 6:30 p.m.]

-got familiar with web app using flask and tornado.
-Pushed Timesheet.


*** <2014-06-04 Wed>
  [In Time: 09:20 a.m.] [Out Time: 6:30 p.m.]

-Made a basic web app using tornado  
-Pushed Timesheet.

*** <2014-06-05 Thur>
  [In Time: 09:45 a.m.] [Out Time: 6:30 p.m.]
-Wrote a code for just with the dummy lab info.
-Solved the problem of get() in tornado handler.
-Pushed Timesheet.

*** <2014-06-06 Fri>
  [In Time: 09:30 a.m.] [Out Time: 6:30 p.m.]
-studied about tornado webserver
-Pushed Timesheet.


*** <2014-06-07 Sat>
  [In Time: 09:30 a.m.] [Out Time: 4:00 p.m.]
-studied and tried to explore ways on how we can access and update data from time to time dynamically.
-pushed timesheet.

*** <2014-06-09 Mon>
  [In Time: 9:45 a.m.] [Out Time: 6:30 p.m.]
-discussion and presentation of the tasks 
-pushed timesheet.

*** <2014-06-10 Tue>
  [In Time: 09:50 a.m.] [Out Time: 6:45 p.m.]
-setting up of ovpl and discussion on further tasks
-pushed timesheet

*** <2014-06-11 Wed>
  [In Time: 09:50 a.m.] [Out Time: 6:45 p.m.]
-Discussed about the tasks to be done in dataservices.
-studying about sqlalchemy.
-pushed timesheet.

*** <2014-06-12 Thur>
  [In Time: 09:50 a.m.] [Out Time: 6:45 p.m.]
-wrote a tornado app for dataservices with all the url's.
-written some modules for generating and parsing url's.
-pushed timesheet.



