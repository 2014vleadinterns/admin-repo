* 2014

** May 2014

*** <2014-05-20 Tues>
  [In Time: 9:00 a.m.] [Out Time: 6:00 p.m.]
  - Attended the bootcamp
  - Learned usage of git .
  - Pushed the timesheet. 

*** <2014-05-21 Wed>
  [In Time: 10:00 a.m.] [Out Time: 5:00 p.m.]
  - attended the bootcamp
  - learned Emacs 
  - learned org mode
  - learned literate programming 
  - attended python session continued
  - pushed the timesheet

*** <2014-05-22 Thur>
  [In Time: 9:00 a.m.] [Out Time: 6:00 p.m.]
  - attended the project presentation session
  - learned about various projects
  - projects assigned
  - completed the test cases and read about exception handling
  - studying about cloud computing
  - pushed the timesheet

*** <2014-05-23 Fri>
  [In Time: 9:00 a.m.] [Out Time: 8:00 p.m.]
  - Had a discussion regarding the progress of the python code
  - Completed the rest of the clipboard code
  - pushed the timesheet

*** <2014-05-24 Sat>
  [In Time: 9:00 a.m.] [Out Time: 5:30 p.m.]
  - Discussed the new task of notes
  - Implemented the notes task
  - pushed the timesheet

*** <2014-05-26 Mon>
  [In Time: 9:00 a.m.] [Out Time: 7:30 p.m.]
  - Had a discussion regarding the progress of the notes code
  - Discussed the new task of 3-address instructions
  - Attended the presentation of virtualization and open stack
  - Installed and deploy virtual labs on ubuntu virtual machine 
  - pushed the timesheet

*** <2014-05-27 Tues>
  [In Time: 9:30 a.m.] [Out Time: 7:30 p.m.]
  - Completed the notes assigment using Pickle and xml.
  - Worked on Python Assignment Three Address Instructions.
  - Attended a seminar on VMWare AND OpenVZ.
  - Pushed the timesheet.

*** <2014-05-28 Wed>
  [In Time: 10:00 a.m.] [Out Time: 8:30 p.m.]
  - Completed third assignment - 3-address code.
  - Worked on open stack by installing a virtul ubuntu on it with mentor.
  - Pushed the timesheet.

*** <2014-05-29 Thru>
  [In Time: 9:30 a.m.] [Out Time: 7:00 p.m.]
  - Discussed about unit testing.
  - Studied about open stack and installed a virtual machine on open stack.
  - Pushed the timesheet.

*** <2014-05-30 Fri>
  [In Time: 9:30 a.m.] [Out Time: 7:00 p.m.]
  - Worked and explored open stack cloud.


** June 2014

*** <2014-06-02 Mon>
  [In Time: 9:30 a.m.] [Out Time: 7:00 pm]
  - Learned about open stack python bindings.
  - Tried to implement the open stack work done by command line through python bindings.  

*** <2014-06-03 Tues>
  [In Time: 9:45 a.m.] [Out Time: 7:00 pm]
  - Documented about the procedure of virtual machine, open stack and instances installation.
  - Completed the open stack installation code in python bindings.

*** <2014-06-04 Wed>
  [In Time:09:45 am] [Out Time: 7:00pm]
- Worked on script to automate instance creation task, using nova n python bindings openstack.
- Wrote script to create volume, snapshots

*** <2014-06-05 Thur>
  [In Time:09:30 am] [Out Time: 7:30pm]
- Worked on script to automate instance creation tast, using python bindings openstack.
- Wrote test cases for the script written.

*** <2014-06-06 Fri>
  [In Time:10:00 am] [Out Time: 7:30pm]
- Worked on script to automate instance creation tast, using python bindings openstack.
- Wrote test cases for the script written.

*** <2014-06-07 Sat>
  [In Time:09:30 am] [Out Time: 2:00pm]
- Worked on using Images in Openstack Web console and on increasing Disk size.

*** <2014-06-09 Mon>
  [In Time:09:30 am] [Out Time: 7:00 pm]
- Presentations . Gave Presentation on Openstack cloud.

*** <2014-06-10 Tue>
  [In Time:09:30 am] [Out Time: 7:30pm]
- Worked Openstack script using python bindings, automating it.

*** <2014-06-11 Wed>
  [In Time:09:30 am] [Out Time: 8:00pm]
- Worked on creating customized images for openstack instances.

*** <2014-06-12 Thur>
  [In Time:09:40 am] [Out Time: 8:00pm]
- Worked on creating customized images for openstack instances.

*** <2014-06-13 Fri>
  [In Time:09:40 am] [Out Time: 8:00pm]
- Worked on changing the ram and disk size and creating the new flavor for an instance.

*** <2014-06-14 Sat>
  [In Time:09:10 am] [Out Time: 12:00pm]
- Preparing Document for Installation and Errors we encountered.

*** <2014-06-16 Mon>
  [In Time:09:05 am] [Out Time: 8:50pm]
- Creating customized Image. Try to solve the errors.

*** <2014-06-17 Tue>
  [In Time:09:05 am] [Out Time: 8:50pm]
- Creating Customized ubuntu image. Unable to ssh into the instance. so, tried to create image with another way.

*** <2014-06-18 Wed>
  [In Time:09:00 am] [Out Time: 8:00pm]
- Prepared wiki page about Installation of Openstack and administrative guide. 
- Prepared presentation.

*** <2014-06-19 Thru>
  [In Time:09:10 am] [Out Time: 8:40pm]
- Modified the code. Code is now able to accept list as input to create, delete, start, stop and restart instances.

*** <2014-06-20 Fri>
  [In Time:09:00 am] [Out Time: 8:50pm]
- Gave Presentation. 
- Modified code and modified test cases as per the code.

*** <2014-06-21 Sat>
  [In Time:09:00 am] [Out Time: 3:00pm]
- Modified code and test cases in object orinted form.

*** <2014-06-23 Mon>
  [In Time:09:40 am] [Out Time: 8:30pm]
- System hard disk crashed on sunday. Tried to recover the lost data. 

*** <2014-06-24 Tue>
  [In Time:09:10 am] [Out Time: 08:30pm]
- Code Review : Modified the code .

*** <2014-06-25 Wed>
  [In Time:09:10 am] [Out Time: 07:30pm]
- Worked on code modification.

*** <2014-06-26 Thurs>
  [In Time:09:00 am] [Out Time: 06:30pm]
- Second code review. Few other modifications were given.

*** <2014-06-27 Fri>
  [In Time:09:00 am] [Out Time: 06:30pm]
- Modified the code as per the given modifications.

*** <2014-07-30 Mon>
  [In Time:09:00 am] [Out Time: 07:30pm]
- Worked on documenting Openstack script and worked on writing paper.

*** <2014-07-01 Tue>
  [In Time:09:20 am] [Out Time: 07:00pm]
- Worked on writing paper.

*** <2014-07-02 Wed>
  [In Time:09:20 am] [Out Time: 07:50pm]
- Worked on writing paper 

*** <2014-07-03 Thur>
  [In Time:09:00 am] [Out Time: 08:00pm]
- Worked on writing paper.                           

*** <2014-07-04 Fri>
  [In Time:09:00 am] [Out Time: 08:30pm]
- Worked on writing paper.

*** <2014-07-05 Sat>
  [In Time:09:00 am] [Out Time: 08:00pm]
- Worked on writing paper.

*** <2014-07-07 Mon>
  [In Time:09:00 am] [Out Time: 07:00pm]
- Paper review was done. Modifying the paper contents.