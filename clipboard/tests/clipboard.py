import sys

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__size__ = None

def copytext(text,size):
    global __text__
    global __size__
    __text__ = text
    __size__ = size

def copyblob(blob,size):
    global __blob__
    global __size__
    __blob__ = blob
    __size__ = size

def gettext():
    global __text__
    return __text__

def getblob():
    global __blob__
    return __blob__

def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None


##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
